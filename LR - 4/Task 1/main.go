package main

import _ "github.com/andlabs/ui/winmanifest"
import (
	"fmt"
	"strconv"

	"github.com/andlabs/ui"
)

func main() {
	err := ui.Main(func() {

		wLabel := ui.NewLabel("Ширина(см)")
		width := ui.NewEntry()
		hLabel := ui.NewLabel("Ширина(см)")
		height := ui.NewEntry()

		mLabel := ui.NewLabel("Матеріал")
		material := ui.NewCombobox()
		material.Append("Дерево")
		material.Append("Метал")
		material.Append("Металопластик")
		material.SetSelected(0)

		pLabel := ui.NewLabel("Склопакет")
		paket := ui.NewCombobox()
		paket.Append("Однокамерний")
		paket.Append("Двокамерний")
		paket.SetSelected(0)

		check := ui.NewCheckbox("Підвіконня")
		button := ui.NewButton("Розрахувати")
		resLabel := ui.NewLabel("Вартість: ")

		box := ui.NewVerticalBox()
		box.Append(wLabel, false)
		box.Append(width, false)
		box.Append(hLabel, false)
		box.Append(height, false)
		box.Append(mLabel, false)
		box.Append(material, false)
		box.Append(pLabel, false)
		box.Append(paket, false)
		box.Append(check, false)
		box.Append(button, false)
		box.Append(resLabel, false)

		window := ui.NewWindow("Склопакет", 300, 200, false)
		window.SetMargined(true)
		window.SetChild(box)

		button.OnClicked(func(*ui.Button) {
			var res float64 = 0
			w, err := strconv.ParseFloat(width.Text(), 64)
			h, err2 := strconv.ParseFloat(height.Text(), 64)
			if err == nil && err2 == nil {
				res = w * h
				if material.Selected() == 0 && paket.Selected() == 0 {
					res = res * 0.25
				}
				if material.Selected() == 0 && paket.Selected() == 1 {
					res = res * 0.3
				}
				if material.Selected() == 1 && paket.Selected() == 0 {
					res = res * 0.05
				}
				if material.Selected() == 1 && paket.Selected() == 1 {
					res = res * 0.1
				}
				if material.Selected() == 2 && paket.Selected() == 0 {
					res = res * 0.15
				}
				if material.Selected() == 2 && paket.Selected() == 1 {
					res = res * 0.2
				}
				if check.Checked() {
					res = res + 35
				}
				r := fmt.Sprintf("%f", res)
				resLabel.SetText("Вартість: " + r + " грн")
			}
		})

		window.OnClosing(func(*ui.Window) bool {
			ui.Quit()
			return true
		})
		window.Show()
	})
	if err != nil {
		panic(err)
	}
}
