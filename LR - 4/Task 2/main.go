package main

import _ "github.com/andlabs/ui/winmanifest"
import (
	"fmt"
	"strconv"

	"github.com/andlabs/ui"
)

func main() {
	err := ui.Main(func() {

		dLabel := ui.NewLabel("Кількість днів")
		days := ui.NewEntry()

		cLabel := ui.NewLabel("Країна")
		country := ui.NewCombobox()
		country.Append("Болгарія")
		country.Append("Германія")
		country.Append("Польща")
		country.SetSelected(0)

		sLabel := ui.NewLabel("Сезон")
		season := ui.NewCombobox()
		season.Append("Літо")
		season.Append("Зима")
		season.SetSelected(0)

		gid := ui.NewCheckbox("Гід")
		lux := ui.NewCheckbox("Номер люкс")
		button := ui.NewButton("Розрахувати")
		resLabel := ui.NewLabel("Вартість: ")

		box := ui.NewVerticalBox()
		box.Append(dLabel, false)
		box.Append(days, false)
		box.Append(cLabel, false)
		box.Append(country, false)
		box.Append(sLabel, false)
		box.Append(season, false)
		box.Append(gid, false)
		box.Append(lux, false)
		box.Append(button, false)
		box.Append(resLabel, false)

		window := ui.NewWindow("Тур на отдих", 300, 200, false)
		window.SetMargined(true)
		window.SetChild(box)

		button.OnClicked(func(*ui.Button) {
			var res float64 = 0
			d, err := strconv.ParseFloat(days.Text(), 64)
			if err == nil {
				if country.Selected() == 0 && season.Selected() == 0 {
					res = 100
				}
				if country.Selected() == 0 && season.Selected() == 1 {
					res = 150
				}
				if country.Selected() == 1 && season.Selected() == 0 {
					res = 160
				}
				if country.Selected() == 1 && season.Selected() == 1 {
					res = 200
				}
				if country.Selected() == 2 && season.Selected() == 0 {
					res = 120
				}
				if country.Selected() == 2 && season.Selected() == 1 {
					res = 180
				}
				if lux.Checked() {
					res = res * 1.2
				}
				if gid.Checked() {
					res = res + 50
				}
				res = res * d
				r := fmt.Sprintf("%f", res)
				resLabel.SetText("Вартість: " + r + " грн")
			}
		})

		window.OnClosing(func(*ui.Window) bool {
			ui.Quit()
			return true
		})
		window.Show()
	})
	if err != nil {
		panic(err)
	}
}
