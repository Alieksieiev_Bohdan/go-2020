
package main
import (
"fmt"
"log"
"net/http"
)
func hello(w http.ResponseWriter, r *http.Request) {
	var login = "dude"
	var password = "12345"
	if r.URL.Path != "/" {
		http.Error(w, "404 not found.", http.StatusNotFound)
		return
	}

	switch r.Method {
	case "GET":
		http.ServeFile(w, r, "form.html")
	case "POST":
		// Call ParseForm() to parse the raw query and update r.PostForm and r.Form.
		if err := r.ParseForm(); err != nil {
			fmt.Fprintf(w, "ParseForm() err: %v", err)
			return
		}

		formLogin := r.FormValue("login")
		formPass := r.FormValue("password")
		if formLogin != login {
			fmt.Fprintf(w, "Login - %s is invalid\n", formLogin)
		}
		if formPass != password{
			fmt.Fprintf(w, "Password - %s is invalid\n", formPass)
		}
		if formLogin == login && formPass == password{
			fmt.Fprintf(w, "Successfully logined")
		}

	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
	}
}

func main() {
	http.HandleFunc("/", hello)
	fmt.Printf("Starting server for testing HTTP POST...\n")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}
