package main

import "math"
import "fmt"
import "math/rand"
import "time"

func main() {
	var a = 1103515245
	var c = 12345
	var m = int(math.Pow(2, 31))
	var seed = 1
	var n = 1000
	var rand = 100
	var arr [1000]int

	i := 0
	for i < 1000 {
		seed = Congruent(a, c, seed, m) % rand // rand
		arr[i] = seed
		i = i + 1
	}

	// Перевірка згенерованого масиву
	i = 0
	fmt.Println("Перевірка згенерованого масиву: ")
	for i < n {
		fmt.Println("arr[", i, "] = ", arr[i])
		i += 1
	}

	///////////////////////////////  статистичну імовірність появи випадкових величин
	fmt.Println("Статистичну імовірність появи випадкових величин: ")
	var statisticalProbability [1000]float64
	i = 0
	for i < n {
		j := 0
		tmp := 0
		for j < n {
			if i != j {
				if arr[i] == arr[j] {
					tmp += 1
				}
			}
			j += 1
		}
		statisticalProbability[i] = StatisticalProbability(float64(tmp), float64(n))
		i += 1
	}

	// Перевірка створенного масиву статичної ймовірності
	i = 0
	for i < n {
		fmt.Println("statisticalProbability[", i, "] = ", statisticalProbability[i])
		i += 1
	}

	/////////////////////////////// математичне сподівання випадкових величин
	i = 1 // У книжці написано із 1
	var MX float64 = 0
	for i < n {
		MX += float64(arr[i]) * float64(statisticalProbability[i])
		i += 1
	}
	fmt.Println("Математичне сподівання випадкових величин: ")
	fmt.Println("MX = ", MX)

	/////////////////////////////// Дисперсія
	i = 1 // У книжці написано із 1
	var DX float64 = 0
	for i < n {
		DX += math.Pow(float64(arr[i])-float64(MX), 2) * statisticalProbability[i]
		i += 1
	}
	fmt.Println("Дисперсія: ")
	fmt.Println("DX = ", DX)

	/////////////////////////////// середньоквадратичним відхиленням
	var bX float64 = StandardDeviation(DX)
	fmt.Println("Середньоквадратичним відхиленням: ")
	fmt.Println("bX = ", bX)

	/////////////////////////////// генераці дійсних чисел

	i = 0
	prev := 0
	tmp := 0
	seed = 1
	var arrPlusAndMine [1000]int
	for i < 1000 {
		seed = Congruent(a, c, seed, m) % rand // rand
		arrPlusAndMine[i] = seed

		tmp = randInt(100) % 2
		if int(tmp) == int(prev) {
			tmp = ReversNumber(tmp)
		}
		prev = tmp

		if int(tmp) == 1 {
			arrPlusAndMine[i] *= int(-1)
		}
		i += 1
	}

	//Перевірка згенерованого масиву
	// i = 0
	// fmt.Println("Перевірка згенерованого масиву: ")
	// for i < n {
	// 	fmt.Println("arrPlusAndMine[", i, "] = ", arrPlusAndMine[i])
	// 	i += 1
	// }
}

func Congruent(a, c, seed, m int) int {
	return int((a*seed + c) % m)
}

func StatisticalProbability(m, n float64) float64 {
	return float64(m / n)
}

func StandardDeviation(DX float64) float64 {
	return math.Sqrt(DX)
}

func randInt(max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return rand.Intn(max)
}

func ReversNumber(plus int) int {
	if plus == 0 {
		plus = 1
	} else if plus == 1 {
		plus = 0
	}
	return plus
}



