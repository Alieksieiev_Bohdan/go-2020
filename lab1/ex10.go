package main

import "fmt"

func main() {
	var chartype int8 = 'R'
	var ukraineI rune = 'Ї'

	fmt.Printf("Code '%c' - %d\n", chartype, chartype)
	fmt.Printf("Code '%c' - %d\n", ukraineI, ukraineI)

	//Задание.
	//1. Вывести украинскую букву 'Ї'
	//2. Пояснить назначение типа "rune"

	// Rune - це Тип. Він займає 32 біта і призначений для подання Unicode CodePoint.
	//  Як аналогію набір англійських символів, закодований в «ASCII», має 128 кодових точок.
	//  Таким чином, може поміститися усередині байта (8 біт).
}
