package main

import (
	"fmt"
	"os"
)

func main() {
	arr := [3][2]float32{}

	for i := 0; i < 2; i += 1 {
		for j := 0; j < 3; j += 1 {
			fmt.Fscan(os.Stdin, &arr[j][i])
			if j != 2 {
				fmt.Printf("cx%d: %f\n", j+1, arr[j][i])
			} else {
				fmt.Printf("f(x1,x2): %f\n", arr[j][i])
			}
		}
	}

	x := arr[0][0]*arr[1][1] - arr[0][1]*arr[1][0]
	x1 := arr[2][0]*arr[1][1] - arr[2][1]*arr[1][0]
	x2 := arr[0][0]*arr[2][1] - arr[0][1]*arr[2][0]
	fmt.Printf("x1: %f\nx2: %f\n", x1/x, x2/x)
}
