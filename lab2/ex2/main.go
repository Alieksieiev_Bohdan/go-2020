// main
package main

import "fmt"
import "./math"

func main() {
	sum := math.Add(1.3, 2.33, -3.2)
	fmt.Println(sum)
}
