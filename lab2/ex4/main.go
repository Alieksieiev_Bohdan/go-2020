// main
package main

import "fmt"
import "./math"

func main() {
	sum := math.Add(1, 2, -3)
	fmt.Println(sum)
}
