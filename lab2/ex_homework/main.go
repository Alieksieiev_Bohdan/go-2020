package main

import "fmt"
import mymath "./math"

func main() {
	a := 1
	b := 2
	c := 8

	fmt.Printf("a,k = %d; b = %d; c,x = %d\n", a, b, c)
	fmt.Printf("Lin(k, x, b) = %f\n", mymath.LinearFunction(float64(a), float64(c), float64(b)))
	fmt.Printf("Min(a, b, c) = %d\nMax(a, b, c) = %d\nAvg(a, b, c) = %f\n", mymath.MinFromThree(a, b, c), mymath.MaxFromThree(a, b, c), mymath.Avg(a, b, c))
}
