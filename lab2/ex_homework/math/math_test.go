package math

import "testing"

func TestMinFromThree(t *testing.T) {
	a := -1
	b := 2
	c := 3
	res := -1
	t_res := MinFromThree(a, b, c)
	if res != t_res {
		t.Errorf("Min(%d,%d,%d) must be %d not %d", a, b, c, res, t_res)
	}
}

func TestMaxFromThree(t *testing.T) {
	a := -1
	b := 2
	c := 3
	res := 3
	t_res := MaxFromThree(a, b, c)
	if res != t_res {
		t.Errorf("Min(%d,%d,%d) must be %d not %d", a, b, c, res, t_res)
	}
}

func TestAvg(t *testing.T) {
	a := -1
	b := 2
	c := 3
	res := float32(4.0 / 3.0)
	t_res := Avg(a, b, c)
	if res != t_res {
		t.Errorf("Min(%d,%d,%d) must be %f not %f", a, b, c, res, t_res)
	}
}

func TestLinearFunction(t *testing.T) {
	k := 2.0
	x := 3.0
	b := 4.0
	res := 10.0
	if t_res := LinearFunction(k, x, b); t_res != res {
		t.Errorf("Lin(%f,%f,%f) must be %f not %f", k, x, b, res, t_res)
	}
}
