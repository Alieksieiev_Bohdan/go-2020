package main

import "fmt"
import mymath "./math"

func main() {
	a := 1
	b := 2
	c := 8

	fmt.Println(a, b, c)
	fmt.Println(mymath.LinearFunction(float64(a), float64(c), float64(b)))
	fmt.Println(mymath.MinFromThree(a, b, c), mymath.MaxFromThree(a, b, c), mymath.Avg(a, b, c))
}
