package math

func MaxFromThree(a, b, c int) int {
	max := a
	if a > max {
		max = a
	}
	if b > max {
		max = b
	}
	if c > max {
		max = c
	}
	return max
}

func MinFromThree(a, b, c int) int {
	min := a
	if a < min {
		min = a
	}
	if b < min {
		min = b
	}
	if c < min {
		min = c
	}
	return min
}

func Avg(a, b, c int) float32 {
	return float32(a+b+c) / float32(3)
}

func LinearFunction(k, x, b float64) float64 {
	return k*x + b
}
